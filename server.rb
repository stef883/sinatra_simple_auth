require 'sinatra'
require 'sinatra/flash'
require 'mongo'

configure do
	enable :sessions
	set :session_secret, "hello_secret"
end

def authenticate
	unless session[:user]
		redirect '/login/'
	end
end

def is_logged_in_user?
	session[:user] ? true : false
end

get '/' do
	erb :index, :layout => :main_layout 
end	

get '/signup/' do
	erb :signup, :layout => :main_layout 
end

get '/login/' do
	if is_logged_in_user?
		flash[:already_logged_in] = "You are already logged in. Redirected to 'profile' page"
		redirect '/profile/'
	else
		erb :login, :layout => :main_layout 
	end
end

get '/profile/' do
	authenticate
	erb :profile, :layout => :main_layout
end

get '/logout/' do
	if is_logged_in_user?
		session.delete(:user)
		flash[:success_logout] = "You successfully logged out"
		redirect '/login/'
	else
		flash[:not_logged_in] = "You are not logged in. Redirected to login page"
		redirect '/login/'
	end
end

post '/signup/' do
	client = Mongo::Client.new('mongodb://127.0.0.1:27017', :database => 'blog')
	users = client[:users]
	doc = { email: params[:email], password: params[:psw]}
	result = users.insert_one(doc)
	if result.n == 1 # find better way to do it
		redirect '/login/'
	end
	result.n
end

post '/login/' do
	client = Mongo::Client.new('mongodb://127.0.0.1:27017', :database => 'blog')
	users = client[:users]
	user = users.find({email: params[:email], password: params[:psw]}).first
	if user
		session[:user] = user[:email]
		flash[:success_login] = "You successfully logged in!"
		redirect '/profile/'
	else
		redirect '/login/'
	end
end